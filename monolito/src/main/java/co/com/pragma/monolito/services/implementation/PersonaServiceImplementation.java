package co.com.pragma.monolito.services.implementation;

import co.com.pragma.monolito.data.entities.PersonaEntity;
import co.com.pragma.monolito.data.repositories.PersonaRepository;
import co.com.pragma.monolito.exceptions.errors.GeneralError;
import co.com.pragma.monolito.services.PersonaService;
import co.com.pragma.monolito.web.dtos.PersonaDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonaServiceImplementation implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Override
    public PersonaDTO getPersonaById(String id) {
        if(personaRepository.existsById(id)){
            return modelMapper.map(personaRepository.findById(id).get(), PersonaDTO.class);
        }
        else{
            throw new GeneralError(HttpStatus.NOT_FOUND, "El usuario no existe");
        }
    }
    @Override
    public List<PersonaDTO> obtenerUserEdad(Integer edad){
        List<PersonaEntity> mayor = personaRepository.findByEdadGreaterThanEqual(edad);
        if(mayor.size()==0) {
            throw new GeneralError(HttpStatus.NOT_FOUND, "No hay usuarios mayores o iguales a la edad " + edad);
        }

        List<PersonaDTO> mayores=new ArrayList<>();

        for(PersonaEntity p:mayor){
            mayores.add(modelMapper.map(p,PersonaDTO.class));
        }
        return mayores;
    }

    @Override
    public PersonaDTO createPersona(PersonaDTO personaDTO) {
        if(personaRepository.existsById(personaDTO.getDocumento())){
            throw new GeneralError(HttpStatus.BAD_REQUEST, "El usuario ya existe");
        }
        else{
            personaRepository.save(modelMapper.map(personaDTO, PersonaEntity.class));
            return personaDTO;
        }
    }

    @Override
    public Boolean deletePersonaBydId(String id) {
        if(personaRepository.existsById(id)){
            personaRepository.deleteById(id);
            return true;
        }
        else{
            throw new RuntimeException("No existe la persona");
        }
    }

    @Override
    public PersonaDTO updatePersona(PersonaDTO personaDTO) {
        if(personaRepository.existsById(personaDTO.getDocumento())){
            personaRepository.save(modelMapper.map(personaDTO, PersonaEntity.class));
            return personaDTO;
        }
        else{
            throw new RuntimeException("El usuario ya existe");
        }
    }
}
