package co.com.pragma.monolito.web.controllers;

import co.com.pragma.monolito.services.PersonaService;
import co.com.pragma.monolito.web.dtos.PersonaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping("/persona")
public class PersonaController {

    @Autowired
    private PersonaService personaService;

    @GetMapping()
    public ResponseEntity<PersonaDTO> getPersona(@RequestParam("id") String id){
        return ResponseEntity.ok(personaService.getPersonaById(id));
    }

    @GetMapping("/buscar")
    public ResponseEntity<List<PersonaDTO>> getPersonaedad(@RequestParam("edad") Integer edad){
        return ResponseEntity.ok(personaService.obtenerUserEdad(edad));
    }

    @PostMapping()
    public ResponseEntity<PersonaDTO> createPersona(@RequestBody PersonaDTO personaDTO){
        return ResponseEntity.ok(personaService.createPersona(personaDTO));
    }

    @PutMapping()
    public ResponseEntity<PersonaDTO> updatePersona(@RequestBody PersonaDTO personaDTO){
        return ResponseEntity.ok(personaService.updatePersona(personaDTO));
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deletePersona(@RequestParam("id") String id){
        return ResponseEntity.ok(personaService.deletePersonaBydId(id));
    }

}
