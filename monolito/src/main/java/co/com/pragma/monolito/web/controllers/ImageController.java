package co.com.pragma.monolito.web.controllers;


import co.com.pragma.monolito.services.ImageService;
import co.com.pragma.monolito.web.dtos.ImageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping("/imagen")
public class ImageController {


    @Autowired
    private ImageService imagenService;

    @GetMapping()
    public ResponseEntity<ImageDTO> getPersona(@RequestParam("id") String id){

        return ResponseEntity.ok(imagenService.getImageById(id));
    }

    @PostMapping()
    public ResponseEntity<ImageDTO> createPersona(@RequestBody ImageDTO imageDTO){
        return ResponseEntity.ok(imagenService.createImage(imageDTO));
    }

    @PutMapping()
    public ResponseEntity<ImageDTO> updatePersona(@RequestBody ImageDTO imageDTO){
        return ResponseEntity.ok(imagenService.updateImage(imageDTO));
    }

    @DeleteMapping()
    public ResponseEntity<Boolean> deletePersona(@RequestParam("id") String id){
        return ResponseEntity.ok(imagenService.deleteImageBydId(id));
    }
}

