package co.com.pragma.monolito.data.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "personas")
public class PersonaEntity {
    private String nombres;
    private String apellidos;
    private Integer tipoDocumento;
    @Id
    private String documento;
    private Integer edad;
    private String ciudadNacimiento;
}
