package co.com.pragma.monolito.exceptions;

import co.com.pragma.monolito.exceptions.errors.GeneralError;
import co.com.pragma.monolito.web.dtos.GeneralErrorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(GeneralError.class)
    public ResponseEntity<GeneralErrorDTO> generalError(GeneralError err){
        return new ResponseEntity(new GeneralErrorDTO(err.getStatus(), err.getMessage()), err.getStatus());
    }

}
