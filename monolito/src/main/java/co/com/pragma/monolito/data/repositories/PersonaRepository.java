package co.com.pragma.monolito.data.repositories;

import co.com.pragma.monolito.data.entities.PersonaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRepository extends CrudRepository<PersonaEntity, String> {

    public List<PersonaEntity> findByEdadGreaterThanEqual(Integer edad);

}
