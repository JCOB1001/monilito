package co.com.pragma.monolito.services;

import co.com.pragma.monolito.web.dtos.PersonaDTO;

import java.util.List;

public interface PersonaService {
    PersonaDTO getPersonaById(String id);
    List<PersonaDTO> obtenerUserEdad(Integer edad);
    PersonaDTO createPersona(PersonaDTO personaDTO);
    Boolean deletePersonaBydId(String id);
    PersonaDTO updatePersona(PersonaDTO personaDTO);
}
