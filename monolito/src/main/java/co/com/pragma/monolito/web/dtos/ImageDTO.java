package co.com.pragma.monolito.web.dtos;

import org.bson.types.Binary;

import lombok.Data;

import java.io.Serializable;
@Data

public class ImageDTO implements Serializable{
    private String id;
    private String tipoImagen;
    private Binary imagen;
    private String personaId;
}
