package co.com.pragma.monolito.web.dtos;

import lombok.Data;

import java.io.Serializable;
@Data
public class PersonaDTO implements Serializable {
    private String nombres;
    private String apellidos;
    private Integer tipoDocumento;
    private String documento;
    private Integer edad;
    private String ciudadNacimiento;
}
