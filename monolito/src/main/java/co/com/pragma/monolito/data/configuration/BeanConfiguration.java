package co.com.pragma.monolito.data.configuration;

import co.com.pragma.monolito.data.repositories.ImagenRepository;
import co.com.pragma.monolito.services.implementation.ImagenServiceImplentation;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {
    @Bean
    ModelMapper modelMapper(){
        return new ModelMapper();
    }
    @Bean
    public ImagenServiceImplentation imagenServiceImplentation(ImagenRepository imagenRepository, ModelMapper modelMapper){
        return new ImagenServiceImplentation(imagenRepository, modelMapper );
    }
}
