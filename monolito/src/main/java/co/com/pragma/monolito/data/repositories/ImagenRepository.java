package co.com.pragma.monolito.data.repositories;

import co.com.pragma.monolito.data.entities.ImagenEntity;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ImagenRepository extends MongoRepository<ImagenEntity, String> {
}

