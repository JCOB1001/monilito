package co.com.pragma.monolito.data.entities;

import lombok.Data;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;




@Data
@Document("imagenes")
public class ImagenEntity {

    @Id
    private String id;
    private String tipoImagen;
    private Binary imagen;
    private String personaId;
}
