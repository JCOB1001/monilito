package co.com.pragma.monolito.services.implementation;

import co.com.pragma.monolito.data.entities.ImagenEntity;
import co.com.pragma.monolito.data.repositories.ImagenRepository;
import co.com.pragma.monolito.exceptions.errors.GeneralError;
import co.com.pragma.monolito.services.ImageService;
import co.com.pragma.monolito.web.dtos.ImageDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
public class ImagenServiceImplentation implements ImageService{


    private final ImagenRepository imageRepository;



    private final ModelMapper modelMapper;
    @Override
    public ImageDTO getImageById(String id) {
        if(imageRepository.existsById(id)){
            return modelMapper.map(imageRepository.findById(id).get(), ImageDTO.class);
        }
        else{
            throw new GeneralError(HttpStatus.NOT_FOUND, "La Imagen no existe");
        }
    }

    @Override
    public ImageDTO createImage(ImageDTO imageDTO) {
        if(imageRepository.existsById(imageDTO.getId())){
            throw new GeneralError(HttpStatus.BAD_REQUEST, "La imagen no existe");
        }
        else{
            imageRepository.save(modelMapper.map(imageDTO, ImagenEntity.class));
            return imageDTO;
        }
    }

    @Override
    public Boolean deleteImageBydId(String id) {
        if(imageRepository.existsById(id)){
            imageRepository.deleteById(id);
            return true;
        }
        else{
            throw new RuntimeException("No existe la imagen");
        }
    }

    @Override
    public ImageDTO updateImage(ImageDTO imageDTO) {
        if(imageRepository.existsById(imageDTO.getId())){
            imageRepository.save(modelMapper.map(imageDTO, ImagenEntity.class));
            return imageDTO;
        }
        else{
            throw new RuntimeException("La imagen ya existe");
        }
    }
}
