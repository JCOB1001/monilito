package co.com.pragma.monolito.services;
import co.com.pragma.monolito.web.dtos.ImageDTO;
import java.util.List;


public interface ImageService {
        ImageDTO getImageById(String id);
        ImageDTO createImage(ImageDTO imageDTO);
        Boolean deleteImageBydId(String id);
        ImageDTO updateImage(ImageDTO imageDTO);
        }

